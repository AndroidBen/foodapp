package androidben.foodapp.services;

import androidben.foodapp.R;
import androidben.foodapp.models.Food;
import androidben.foodapp.models.FoodCommand;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Ben on 23/01/2017.
 */
public interface PostFoodService {


    @POST("food/adress")
    Call<FoodCommand> createFood(@Body FoodCommand food);

}
