package androidben.foodapp.activity;

import android.content.pm.ActivityInfo;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import androidben.foodapp.FoodApplication;
import androidben.foodapp.R;
import androidben.foodapp.fragment.FoodListFragment;
import androidben.foodapp.models.Food;

public class FoodListActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);

        if (!FoodApplication.getApp().isTablet())
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        if (savedInstanceState == null)
        {
            FragmentManager mgr = getSupportFragmentManager();
            FragmentTransaction transaction = mgr.beginTransaction();

            FoodListFragment foodListFragment = new FoodListFragment();

            transaction.replace(R.id.food_list_container, foodListFragment);

            transaction.commit();
        }
    }
}
