package androidben.foodapp.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ben on 23/01/2017.
 */
public class Food implements Parcelable {

    private String name;
    private float price;
    private Bitmap picture;


    public Food(String name, float price, Bitmap picture) {
        this.name = name;
        this.price = price;
        this.picture = picture;
    }


    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public Bitmap getPicture() {
        return picture;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(picture, 0);
        dest.writeString(name);
        dest.writeFloat(price);
    }

    public static final Parcelable.Creator<Food> CREATOR
            = new Parcelable.Creator<Food>() {
        public Food createFromParcel(Parcel in) {
            return new Food(in);
        }

        public Food[] newArray(int size) {
            return new Food[size];
        }
    };

    private Food(Parcel in) {
        picture = Bitmap.CREATOR.createFromParcel(in);
        name = in.readString();
        price = in.readFloat();
    }
}
