package androidben.foodapp.models;

/**
 * Created by Ben on 23/01/2017.
 */
public class FoodCommand {

    private String name;
    private float price;


    public FoodCommand(Food food) {
        this.name = food.getName();
        this.price = food.getPrice();
    }
}
