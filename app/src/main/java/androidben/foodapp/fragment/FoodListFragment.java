package androidben.foodapp.fragment;

import android.content.pm.ActivityInfo;
import android.content.pm.ConfigurationInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidben.foodapp.R;
import androidben.foodapp.activity.FoodListActivity;
import androidben.foodapp.adapter.FoodListAdapter;
import androidben.foodapp.models.Food;

/**
 * Created by Ben on 23/01/2017.
 */
public class FoodListFragment extends Fragment implements AdapterView.OnItemClickListener {

    protected FoodListActivity activity;
    protected FoodListAdapter adapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (FoodListActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_food_list, container, false);

        RecyclerView foodList =
                (RecyclerView) v.findViewById(R.id.fragment_food_list_recycler_view);

        foodList.setLayoutManager(new LinearLayoutManager(activity));
        this.adapter = new FoodListAdapter(activity, this);
        foodList.setAdapter(adapter);

        return v;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        FoodDetailFragment foodDetailFragment = new FoodDetailFragment();
        Bundle bundle = new Bundle();
        Food food = adapter.getItem(position);
        bundle.putParcelable(FoodDetailFragment.FOOD, food);
        foodDetailFragment.setArguments(bundle);

        FragmentManager mgr = activity.getSupportFragmentManager();
        FragmentTransaction transaction = mgr.beginTransaction();

        int orientation = activity.getResources().getConfiguration().orientation;

        if (orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            transaction.replace(R.id.food_details_container, foodDetailFragment);
        } else {
            transaction.replace(R.id.food_list_container, foodDetailFragment);
        }
        transaction.addToBackStack("food list");
        transaction.commit();

    }
}
