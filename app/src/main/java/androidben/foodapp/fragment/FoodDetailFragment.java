package androidben.foodapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import androidben.foodapp.R;
import androidben.foodapp.models.Food;
import androidben.foodapp.models.FoodCommand;
import androidben.foodapp.services.PostFoodService;
import androidben.foodapp.services.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ben on 23/01/2017.
 */
public class FoodDetailFragment extends Fragment implements View.OnClickListener {

    public static final String FOOD = "food";

    private Food food;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null)
            food = bundle.getParcelable(FOOD);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_food_details, container, false);

        ImageView imageView = (ImageView) v.findViewById(R.id.fragment_food_details_image);
        imageView.setImageBitmap(food.getPicture());

        TextView nameTextView = (TextView) v.findViewById(R.id.fragment_food_details_name);
        nameTextView.setText(food.getName());

        TextView priceTextView = (TextView) v.findViewById(R.id.fragment_food_details_price);
        priceTextView.setText(String.format(getString(R.string.price),
                String.valueOf(food.getPrice())));

        Button commandButton = (Button) v.findViewById(R.id.fragment_food_details_command_button);
        commandButton.setOnClickListener(this);

        return v;
    }


    @Override
    public void onClick(View v) {

        PostFoodService service = ServiceGenerator.createService(PostFoodService.class);

        Call<FoodCommand> call = service.createFood(new FoodCommand(this.food));
        call.enqueue(new Callback<FoodCommand>() {
            @Override
            public void onResponse(Call<FoodCommand> call, Response<FoodCommand> response) {
                if (response.isSuccessful())
                {
                    Toast.makeText(getContext(),
                            getString(R.string.success),
                            Toast.LENGTH_SHORT)
                            .show();
                }
                else
                {
                    Toast.makeText(getContext(),
                            getString(R.string.fail),
                            Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<FoodCommand> call, Throwable t) {
                Toast.makeText(getContext(),
                        getString(R.string.fail),
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

    }
}
