package androidben.foodapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import androidben.foodapp.R;
import androidben.foodapp.fragment.FoodListFragment;
import androidben.foodapp.models.Food;

/**
 * Created by Ben on 23/01/2017.
 */
public class FoodListAdapter extends RecyclerView.Adapter<FoodListAdapter.ViewHolder> {

    private WeakReference<Context> context = new WeakReference<Context>(null);
    private FoodListFragment foodListFragment;
    private List<Food> foodList;
    private int foodListSize;

    public FoodListAdapter(Context ctx, FoodListFragment flf) {

        this.context = new WeakReference<Context>(ctx);
        this.foodListFragment = flf;
        initFoodList();
    }


    private void initFoodList() {
        this.foodList = new ArrayList<>();

        Resources resources = context.get().getResources();
        Bitmap muffin = BitmapFactory.decodeResource(resources, R.drawable.android_muffin);
        Bitmap donut = BitmapFactory.decodeResource(resources, R.drawable.android_donut);

        for (int i = 0; i < 25; i++)
        {
            if (i % 2 == 0)
                foodList.add(new Food("food_" + i, (float) (i + 0.5), muffin));
            else
                foodList.add(new Food("food_" + i, (float) (i + 0.5), donut));
        }


        this.foodListSize = foodList.size();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context.get()).inflate(R.layout.cell_food_list, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        Food food = foodList.get(position);
        String priceString = context.get().getString(R.string.price);

        holder.imageView.setImageBitmap(food.getPicture());
        holder.nameTextView.setText(food.getName());
        holder.priceTextView.setText(String.format(priceString, String.valueOf(food.getPrice())));

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foodListFragment.onItemClick(null, v, position, v.getId());
            }
        });
    }


    @Override
    public int getItemCount() {
        return foodListSize;
    }


    public Food getItem(int position)
    {
        return foodList.get(position);
    }


    public void addFoodItem(Food food)
    {
        this.foodList.add(food);
        this.foodListSize++;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        protected ImageView imageView;
        protected TextView nameTextView;
        protected TextView priceTextView;
        protected View view;

        public ViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.cell_food_image);
            nameTextView = (TextView) itemView.findViewById(R.id.cell_food_name);
            priceTextView = (TextView) itemView.findViewById(R.id.cell_food_price);
        }
    }
}
