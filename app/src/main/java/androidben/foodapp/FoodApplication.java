package androidben.foodapp;

import android.app.Application;

/**
 * Created by Ben on 23/01/2017.
 */
public class FoodApplication extends Application {

    private static FoodApplication foodApplication;
    private boolean tablet;

    @Override
    public void onCreate() {
        super.onCreate();
        foodApplication = this;
        tablet = getResources().getBoolean(R.bool.is_tablet);
    }

    public static FoodApplication getApp() {
        return foodApplication;
    }

    public boolean isTablet() {
        return tablet;
    }
}
